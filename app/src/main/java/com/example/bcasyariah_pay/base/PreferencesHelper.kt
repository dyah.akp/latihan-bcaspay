package com.example.bcasyariah_pay.base

import android.content.SharedPreferences
import javax.inject.Inject

class PreferencesHelper @Inject constructor(private  val sharedPreferences: SharedPreferences) {

    fun saveName(value: String){
        sharedPreferences.edit().putString(KEY_NAME, value).apply()
    }

    fun getName() : String?{
        return sharedPreferences.getString(KEY_NAME,"-")
    }
    fun saveAddress(value : String){
        sharedPreferences.edit().putString(KEY_ADDRESS,value).apply()
    }

    fun getAddress() : String?{
        return sharedPreferences.getString(KEY_ADDRESS,"-")
    }

    fun clearDataPref(){
        sharedPreferences.edit().clear().apply()
    }
    companion object {
        const val  KEY_NAME ="key name"
        const val  KEY_ADDRESS ="key address"
    }

}