package com.example.bcasyariah_pay.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.bcasyariah_pay.databinding.ActivityRegisterBinding
import com.example.bcasyariah_pay.view.login.LoginActivity

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvAlreadyHaveAccount.setOnClickListener {
            val intent = Intent (applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }
    }
}