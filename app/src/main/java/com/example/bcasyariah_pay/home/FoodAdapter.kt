package com.example.bcasyariah_pay.home


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bcasyariah_pay.databinding.ItemFoodBinding
import com.example.bcasyariah_pay.home.model.FoodsModel


class FoodAdapter(
//    private val dataFoods: List<FoodsModel>,
//    private val onClickFoods : (FoodsModel) -> Unit  //untuk ambil details

//    mengembalikan suatu hal atau tidak = lamda

) : RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    private var dataFood: MutableList<FoodsModel> = mutableListOf()

    fun setData(newData : MutableList<FoodsModel>){
        dataFood = newData
        notifyDataSetChanged()
    }


    inner class FoodViewHolder(val binding: ItemFoodBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: FoodsModel) {
            Glide.with(binding.root.context)
                .load(data.image)
                .into(binding.ivItemFood)
            binding.tvTitleCard.text = data.title
            binding.tvDescCard.text = data.desc
            binding.lvFood.setOnClickListener { //kalau data diserver ga ada dikasih default apa
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder =
        FoodViewHolder(
            ItemFoodBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        holder.bindView(dataFood[position]) //slot keseluruhan tangga, jd array harus ada object

    }

    override fun getItemCount(): Int = dataFood.size

}