package com.example.bcasyariah_pay.home.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FoodsModel(
    val image: String?, //drawable integer  , server : string harus nullable
    val title: String,
    val desc: String?,
    val category  : String?
    //2. setelah itu ngisi data
) : Parcelable   //ngeparse ke activity baru
