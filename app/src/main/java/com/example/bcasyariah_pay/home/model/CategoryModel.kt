package com.example.bcasyariah_pay.home.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CategoryModel(
    val imageCategory: Int?, //drawable integer  , server : string harus nullable
    val imageSmallCategory: Int?,
    val title: String?,
    val desc: String?,
) : Parcelable
