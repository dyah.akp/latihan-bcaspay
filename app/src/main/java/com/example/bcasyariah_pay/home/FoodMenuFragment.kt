package com.example.bcasyariah_pay.home

import androidx.fragment.app.Fragment
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bcasyariah_pay.databinding.FragmentFoodMenuBinding
import com.example.bcasyariah_pay.home.model.CategoryFoodModel
import com.example.bcasyariah_pay.home.model.FoodsModel

class FoodMenuFragment : Fragment() {
    private var binding: FragmentFoodMenuBinding? = null
    private var foodAdapter = FoodAdapter()
    private var categoryFoodAdapter = CategoryFoodAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFoodMenuBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Food Adapter
        foodAdapter.setData(populateData())
        categoryFoodAdapter.setData(populateDataCategory())

        //Category Food Adapter
        binding?.rvFood?.adapter = foodAdapter
        binding?.rvCategoryFood?.adapter = categoryFoodAdapter

        categoryFoodAdapter.addOnClickFoodCategoryItem { categoryFoodModel ->
            val categoryFoodData = populateDataCategory()

            categoryFoodData.map {
                val selectedId = it.id == categoryFoodModel.id
                it.isSelected = selectedId
            }

            categoryFoodAdapter.setData(categoryFoodData)

            val data = populateData()
            val filterData = data.filter { dataFood ->
                dataFood.category == categoryFoodModel.title
            }
            //untuk filter sesuai category
            foodAdapter.setData(filterData.toMutableList())
        }



        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                var dataFoods = populateData()
                val filterData = dataFoods.filter { dataFoods ->
                    dataFoods.title.contains(p0.toString(), ignoreCase = true)

                }
                foodAdapter.setData(filterData.toMutableList())

                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true) {
                    binding?.ivClose?.visibility = View.INVISIBLE

                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                }


            }


        })

        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.setText("")
        }
    }

    private fun populateData(): MutableList<FoodsModel> {
        val listData = mutableListOf(
            FoodsModel(
                image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1_SkX44fd6C9hMkS9acsDrGj4vRQYlqUjcHPpxlunqQ&s",
                title = "sarapan 1",
                desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Pedas"
            ),
            FoodsModel(
                image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1_SkX44fd6C9hMkS9acsDrGj4vRQYlqUjcHPpxlunqQ&s",
                title = "sarapan 2",
                desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Manis"
            ),
            FoodsModel(
                image = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1_SkX44fd6C9hMkS9acsDrGj4vRQYlqUjcHPpxlunqQ&s",
                title = "sarapan 3",
                desc = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                category = "Pedas"
            )
        )
        return listData
    }

    private fun populateDataCategory(): MutableList<CategoryFoodModel> {
        val listData = mutableListOf(
            CategoryFoodModel(
                id = 1,
                title = "Manis",
                isSelected = false
            ),
            CategoryFoodModel(
                id = 2,
                title = "Pedas",
                isSelected = false
            ),
            CategoryFoodModel(
                id = 3,
                title = "Asam",
                isSelected = false
            ),
            CategoryFoodModel(
                id = 4,
                title = "Asin",
                isSelected = false
            )
        )
        return listData
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}