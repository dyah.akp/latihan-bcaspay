package com.example.bcasyariah_pay.home.model

import android.media.Image
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsModel(
    val image: Int?, //drawable integer  , server : string harus nullable
    val title: String?,
    val desc: String?
    //2. setelah itu ngisi data
) : Parcelable   //ngeparse ke activity baru
