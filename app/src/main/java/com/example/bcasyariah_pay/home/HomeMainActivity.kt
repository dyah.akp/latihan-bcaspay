package com.example.bcasyariah_pay.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.bcasyariah_pay.R
import com.example.bcasyariah_pay.databinding.ActivityHomeMainBinding
import com.example.bcasyariah_pay.home.model.CategoryModel
import com.example.bcasyariah_pay.home.model.NewsModel
import com.example.bcasyariah_pay.view.DetailCategoryActivity
import com.example.bcasyariah_pay.view.DetailNewsActivity

class HomeMainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeMainBinding.inflate(layoutInflater)

//        binding.rvNews.adapter = HomeMainAdapter() //bisa ini cara ambil data
        setContentView(binding.root)
//        binding.componentAppBar.ivBack.visibility = View.GONE
        binding.topBar.tvAppBar.text = "Recycle New Activity"
        binding.topBar.ivBack.setOnClickListener {
            this.onBackPressed()
        }
        val mainAdapter = HomeMainAdapter(
            dataNews = populateData(),
            onClickNews = { dataNews ->
//                Toast.makeText(applicationContext,it.toString, Toast.LENGTH_LONG).show()//bikin object dul harus tring

                DetailNewsActivity.navigateToActivityDetail(this, dataNews)
            })


        val categoryMainAdapter = CategoryMainAdapter(
            dataCategory = populateDataForCategory(),
            onClickCategory = { dataCategory ->
                DetailCategoryActivity.navigateToActivityDetailCategory(this, dataCategory)
            })

        binding.rvListHorizontal.adapter = categoryMainAdapter
        binding.rvNews.adapter = mainAdapter
    }


    private fun populateData(): List<NewsModel> {
        //dibuat list 3
        val listData = listOf(
            NewsModel(
                //implement dr news model
                image = R.drawable.news1,
                title = "Attack Of Cat Kigdom",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            NewsModel(
                image = R.drawable.news2,
                title = "Tiger Swimming In The Sea",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            NewsModel(
                image = R.drawable.news3,
                title = "Elepehant Across The Roads",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            NewsModel(
                image = R.drawable.news4,
                title = "Girrafe Eating Grass",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            NewsModel(
                image = R.drawable.news5,
                title = "Rabbits are Excited",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            NewsModel(
                image = R.drawable.news6,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )

        )
        //4 membutuhkan return, kalau pake api tinggal ditampilkan sesuai respon
        return listData
    }

    private fun populateDataForCategory(): List<CategoryModel> {
        val listData = listOf(
            CategoryModel(
                imageCategory = R.drawable.news1,
                imageSmallCategory = R.drawable.news1,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
            CategoryModel(
                imageCategory = R.drawable.news2,
                imageSmallCategory = R.drawable.news2,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
            CategoryModel(
                imageCategory = R.drawable.news3,
                imageSmallCategory = R.drawable.news3,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
            CategoryModel(
                imageCategory = R.drawable.news4,
                imageSmallCategory = R.drawable.news4,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
            CategoryModel(
                imageCategory = R.drawable.news5,
                imageSmallCategory = R.drawable.news5,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
            CategoryModel(
                imageCategory = R.drawable.news6,
                imageSmallCategory = R.drawable.news6,
                title = "Dogs Are Happy",
                desc = "Lorem Ipsum is simply has been the industry's standard dummy text ever since the 1500s"
            ),
        )
        return listData
    }
}