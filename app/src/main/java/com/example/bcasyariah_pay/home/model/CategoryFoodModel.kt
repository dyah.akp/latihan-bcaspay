package com.example.bcasyariah_pay.home.model

class CategoryFoodModel (
    val id: Int?,
    val title: String?,
    var isSelected: Boolean?
)