package com.example.bcasyariah_pay.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcasyariah_pay.databinding.ItemCategoryBinding
import com.example.bcasyariah_pay.home.model.CategoryModel
class CategoryMainAdapter(
    private val dataCategory: List<CategoryModel>,
    private val onClickCategory : (CategoryModel) -> Unit
) : RecyclerView.Adapter<CategoryMainAdapter.CategoryViewHolder>() {
    inner class CategoryViewHolder(val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(
            binding.root
        ) {
        fun bindingData(data: CategoryModel, onClickCategory: (CategoryModel) -> Unit) {
            binding.ivCategory.setImageResource(data.imageCategory?:0)
            binding.ivSmallCategory.setImageResource(data.imageSmallCategory?:0)
            binding.tvTitleCategory.text = data.title
            binding.tvDescCategory.text = data.desc
            binding.cvCategory.setOnClickListener{
                onClickCategory(data) //kalau data diserver ga ada dikasih default apa
            }

        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder =
       CategoryViewHolder(
           ItemCategoryBinding.inflate(
               LayoutInflater.from(parent.context),
               parent,
               false
           )
       )
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
       holder.bindingData(dataCategory[position], onClickCategory)
    }
    override fun getItemCount(): Int = dataCategory.size



}