package com.example.bcasyariah_pay.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcasyariah_pay.databinding.ItemNewsBinding
import com.example.bcasyariah_pay.home.model.NewsModel

class HomeMainAdapter(

    private val dataNews: List<NewsModel>,
    private val onClickNews : (NewsModel) -> Unit  //untuk ambil details

//    mengembalikan suatu hal atau tidak = lamda

) : RecyclerView.Adapter<HomeMainAdapter.HomeViewHolder>() {
    inner class HomeViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: NewsModel, onClickNews: (NewsModel) -> Unit){
            binding.ivItemNews.setImageResource( data.image?:0)
            binding.tvTitleCard.text = data.title
            binding.tvDescCard.text = data.desc
            binding.lvNews.setOnClickListener{
                onClickNews(data) //kalau data diserver ga ada dikasih default apa
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder =
        HomeViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bindView(dataNews[position],onClickNews) //slot keseluruhan tangga, jd array harus ada object

    }

    override fun getItemCount(): Int = dataNews.size


}