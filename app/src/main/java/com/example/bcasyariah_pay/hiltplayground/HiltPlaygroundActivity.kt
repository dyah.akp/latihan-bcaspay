package com.example.bcasyariah_pay.hiltplayground

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasyariah_pay.R
import com.example.bcasyariah_pay.base.PreferencesHelper
import com.example.bcasyariah_pay.databinding.ActivityHiltPlaygroundBinding
import com.example.bcasyariah_pay.home.model.Engine
import com.example.bcasyariah_pay.home.model.FoodsModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class HiltPlaygroundActivity : AppCompatActivity() {

    //1
    private var _binding: ActivityHiltPlaygroundBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var preferenceHelper: PreferencesHelper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHiltPlaygroundBinding.inflate(layoutInflater)
        setContentView(binding.root)




        binding.btnProfileEdit.setOnClickListener {
            val intent = Intent(this, ProfileOverviewActivity::class.java)
            startActivity(intent)
        }
//        val engine = Engine()
//        engine.startEngine()
//
//        engine.startEngine()


        binding.btnProfileClear.setOnClickListener {
            preferenceHelper.clearDataPref()
            updateValueLayout()
        }

    }
    private fun updateValueLayout(){
        val getNameFromLocal = preferenceHelper.getName()
        val getAddressFromLocal = preferenceHelper.getAddress()

        binding.tvProfileName.text = getNameFromLocal
        binding.tvProfileAddress.text = getAddressFromLocal
    }
}