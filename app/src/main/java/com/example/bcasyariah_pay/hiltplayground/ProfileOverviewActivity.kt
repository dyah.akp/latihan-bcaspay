package com.example.bcasyariah_pay.hiltplayground

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasyariah_pay.base.PreferencesHelper
import com.example.bcasyariah_pay.databinding.ActivityHiltPlaygroundBinding
import com.example.bcasyariah_pay.databinding.ActivityInputProfileBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class ProfileOverviewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInputProfileBinding

    @Inject
lateinit var preferencesHelper: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInputProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSave.setOnClickListener {
            val nameValue = binding.etInputName.text
            val addressValue = binding.etInputAddress.text

            if (nameValue.isNullOrEmpty().not() && addressValue.isNullOrEmpty().not()) {
                preferencesHelper.saveName(nameValue.toString())
                preferencesHelper.saveAddress(addressValue.toString())
                navigateBack()

            } else {
                Toast.makeText(this, "ISI DULU LAH", Toast.LENGTH_LONG).show()
            }

        }
    }


    private fun navigateBack() {
        startActivity(Intent(this, HiltPlaygroundActivity::class.java))

    }
}