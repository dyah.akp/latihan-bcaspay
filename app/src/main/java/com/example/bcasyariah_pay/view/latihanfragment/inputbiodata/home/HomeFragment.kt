package com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.bcasyariah_pay.R
import com.example.bcasyariah_pay.databinding.HomeFragmentBinding
import com.example.bcasyariah_pay.home.FoodMenuFragment
import com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.InputBiodataFragment

class HomeFragment: Fragment() {

    private lateinit var binding: HomeFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnRegister.setOnClickListener {
            navigateToRegister()
        }
        binding.btnLogin.setOnClickListener {
            navigateToFood()
        }
        setApp()
    }
    //setiap fragment di destroy
    private fun setApp(){
        //halaman home tidak butuh app bar
        binding.componentAppBar.ivBack.isVisible = false
    }

    private fun navigateToRegister(){
        val registerFragment = InputBiodataFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, registerFragment)
            .addToBackStack(REGISTER_FRAGMENT_KEY)
            .commit()
    }
    private fun navigateToFood(){
        val foodMenuFragment = FoodMenuFragment()
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, foodMenuFragment)
            .addToBackStack(REGISTER_FRAGMENT_KEY)
            .commit()
    }

    companion object{
        const val REGISTER_FRAGMENT_KEY = "register"
        const val FOOD_FRAGMENT_KEY = "food menu"
    }

}