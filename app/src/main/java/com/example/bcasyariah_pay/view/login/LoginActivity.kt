package com.example.bcasyariah_pay.view.login

import android.content.Intent
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity

import com.example.bcasyariah_pay.databinding.ActivityLoginBinding

import com.example.bcasyariah_pay.view.profile.ProfileActivity

class LoginActivity:AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    val email ="dyah@gmail.com"
    private val name="dyah"
    private val address="jl blablablabla"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnLogin.setOnClickListener {
            val email=binding.etEmail.text.toString()
            navigationScreenWithInput(ProfileActivity::class.java,email)
//            if(email=="swieta@gmail.com"){
//
//                Toast.makeText(applicationContext, "berhasil login", Toast.LENGTH_SHORT).show()
            // val intent = Intent(applicationContext, BiodataActivity::class.java)
//               navigationScreen(ProfileActivity::class.java)


//            }else{
//                Toast.makeText(applicationContext, "gagal login", Toast.LENGTH_SHORT).show()
//            }

        }


    }


    private  fun navigationScreen(screen:Class<*>){
        val intent=Intent(applicationContext,screen)
        intent.putExtra(KEY_NAME,name)
        intent.putExtra(KEY_ADDRESS,address)
        startActivity(intent)



    }

    private fun navigationScreenWithInput(screen: Class<*>,input:String){
        val intent=Intent(applicationContext,screen)
        intent.putExtra(KEY_INPUT,input)
        startActivity(intent)

    }
    ////    untuk yang sifatanya statis
    companion object{
        const val  KEY_NAME="name"
        const val KEY_ADDRESS="address"
        const val KEY_INPUT="Input"

    }
}