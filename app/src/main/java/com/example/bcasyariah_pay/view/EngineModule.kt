package com.example.bcasyariah_pay.view

import com.example.bcasyariah_pay.home.model.Engine
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object EngineModule {

    @Singleton
    @Provides
    fun providesEngine() : Engine{
        return  Engine()
    }
}