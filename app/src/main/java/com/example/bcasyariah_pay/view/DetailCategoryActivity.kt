package com.example.bcasyariah_pay.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasyariah_pay.databinding.ActivityDetailCategoryBinding

import com.example.bcasyariah_pay.home.model.CategoryModel


class DetailCategoryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailCategoryBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailCategoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetail()
    }

    //membuat kunci pada setiap page

    private fun setDataToViewDetail(){
        val data = intent.getParcelableExtra<CategoryModel>(DATA_CATEGORY)
        binding.ivDetailCategory.setImageResource(data?.imageCategory?:0)
        binding.tvDetailTitleNews.text = data?.title
        binding.tvDetailCategory.text = data?.desc
    }


    companion object {
        //kuncinya untuk page ini aja
        private const val DATA_CATEGORY = "dataCategory"
        fun navigateToActivityDetailCategory(
            activity: Activity, dataCategory: CategoryModel
        ) = Intent().apply { //scoping function, lamda mengembalikan objek
            //tidak usah pake intent.putextra
//            putExtra(DATA_NEWS, dataNews ) //key
            val intent = Intent(activity, DetailCategoryActivity::class.java)
            intent.putExtra(DATA_CATEGORY,dataCategory)
            activity.startActivity(intent)
        }
    }
}