package com.example.bcasyariah_pay.view.latihanfragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasyariah_pay.R
import com.example.bcasyariah_pay.databinding.ActivityHostBinding
import com.example.bcasyariah_pay.databinding.ActivityListBinding
import com.example.bcasyariah_pay.databinding.InputBiodataFragmentBinding
import com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.InputBiodataFragment
import com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.home.HomeFragment

class HostActivity : AppCompatActivity() {
    //sebagai penampung
    //2. habis buat layout host activity
    //1. input biodata fragment

    private lateinit var binding: ActivityHostBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //2 (a) dipanggil
        initBiodataFragment()

    }

    //1 (a)
    //2 (b) --> success register layout
    private fun initBiodataFragment(){
        val homeFragment = HomeFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, homeFragment)
            .commit()
    }
}