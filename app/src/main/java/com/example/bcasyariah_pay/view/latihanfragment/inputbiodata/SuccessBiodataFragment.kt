package com.example.bcasyariah_pay.view.latihanfragment.inputbiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.bcasyariah_pay.databinding.SuccessRegisterFragmentBinding
import com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.InputBiodataFragment.Companion.EMAIL_KEY
import com.example.bcasyariah_pay.view.latihanfragment.inputbiodata.InputBiodataFragment.Companion.NAME_KEY

class SuccessBiodataFragment: Fragment() {
    //b
    private lateinit var binding: SuccessRegisterFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= SuccessRegisterFragmentBinding.inflate(layoutInflater,container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //b3 9 panggil function
        setDataFromArguments()

        //b3 8 dari setelah input biodata frgament bikin function get name from argument

    }

    private fun setDataFromArguments(){
        var getNameFromArguments = arguments?.getString(NAME_KEY)
        var getEmailFromArguments = arguments?.getString(EMAIL_KEY)
        binding.tvName.text = getNameFromArguments
        binding.tvEmail.text = getEmailFromArguments
    }

    //b setelah itu input biodata fragment
}