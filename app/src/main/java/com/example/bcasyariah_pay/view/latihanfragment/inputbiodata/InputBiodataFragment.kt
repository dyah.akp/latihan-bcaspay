package com.example.bcasyariah_pay.view.latihanfragment.inputbiodata

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.DEFAULT_ARGS_KEY
import com.example.bcasyariah_pay.R
import com.example.bcasyariah_pay.databinding.InputBiodataFragmentBinding

class InputBiodataFragment : Fragment() { //1

    //3
    private lateinit var binding: InputBiodataFragmentBinding

    override fun onCreateView( //2
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //4 return dihapus diganti binding + return
        binding = InputBiodataFragmentBinding.inflate(inflater, container, false)
        return binding.root


    }

    //5 bikin oncreateview ketika view sudah dibuat
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //6 ke host activity fragment lagi , nempelin fragment" yg sudah dibuat(a)

        //ngubah nama app bar menu profile menjadi profile
        binding.layoutComponentAppBar.tvAppBar.text = "Input Biodata"
        //b 2
        binding.btnSubmit.setOnClickListener {
            handleSubmitButton()
        }
        binding.layoutComponentAppBar.ivBack.setOnClickListener{
            requireActivity().onBackPressed()
        }

    }

    //b 3
    private fun handleSubmitButton() {
        val checkEtName = binding.etName.text.isNullOrBlank().not()
        val checkEtAddress = binding.etAddress.text.isNullOrBlank().not()
        val checkEtPhone= binding.etNoHp.text.isNullOrBlank().not()
        val checkEtEmail= binding.etNoHp.text.isNullOrBlank().not()
        if (checkEtName && checkEtAddress && checkEtPhone && checkEtEmail) {
            //b3 4
            //bundle putExtra app compat , ngebundling data yang akan dikirim melalui arguments
            //kalau di succes
            var bundle = Bundle().apply {
                //b3 6 di parameter put string
                putString(NAME_KEY, binding.etName.text.toString())
                putString(EMAIL_KEY, binding.etEmail.text.toString())
//                kalau mau pake email ditampilin
//                putString(NAME_KEY, binding.etEmail.text.toString()) bikin key lagi di companion objeck bawah
            }
            // b3 3 + b3 6 //fragment butuh argument kalau activity intent.
            val successFragment = SuccessBiodataFragment().apply {
                arguments = bundle
            }
            //b3 7  //b3 8 masuk ke succes biodata fragment
            parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, successFragment)
                .commit()

        } else {
            Toast.makeText(context, "Tolong Lengkapi Biodata", Toast.LENGTH_LONG).show()
        }

    }

    //b3 5 kasih key
    companion object {
        const val NAME_KEY = "name"
        const val EMAIL_KEY = "email"
    }

}