package com.example.bcasyariah_pay.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasyariah_pay.databinding.ActivityDetailNewsBinding
import com.example.bcasyariah_pay.databinding.ActivityRegisterBinding
import com.example.bcasyariah_pay.home.model.NewsModel

class DetailNewsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetail()
    }

    //membuat kunci pada setiap page

    private fun setDataToViewDetail(){
        val data = intent.getParcelableExtra<NewsModel>(DATA_NEWS)
        binding.ivDetailNews.setImageResource(data?.image?:0)
        binding.tvDetailTitleNews.text = data?.title
        binding.tvDetailNews.text = data?.desc
        binding.componentAppBar.tvAppBar.text= data?.title
        binding.componentAppBar.ivBack.setOnClickListener{
           onBackPressed()

        }

    }


    companion object {
        //kuncinya untuk page ini aja
        private const val DATA_NEWS = "dataNews"
        fun navigateToActivityDetail(
            activity: Activity, datanews: NewsModel) = Intent().apply { //scoping function, lamda mengembalikan objek
            //tidak usah pake intent.putextra
//            putExtra(DATA_NEWS, dataNews ) //key
                val intent = Intent(activity, DetailNewsActivity::class.java)
            intent.putExtra(DATA_NEWS,datanews)
            activity.startActivity(intent)
        }
    }
}